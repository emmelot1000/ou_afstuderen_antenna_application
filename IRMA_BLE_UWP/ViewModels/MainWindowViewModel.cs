﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Windows.UI.Core;
using Windows.ApplicationModel.Core;
using IRMA_BLE_UWP.Models;

namespace IRMA_BLE_UWP.ViewModels
{
	/// <summary>
	/// Class for the main window viewmodel to handle all UI tasks
	/// </summary>
	public class MainWindowViewModel : BaseViewModel
	{
		const string END_URL = "URLEND";
		const int MESSAGE_SIZE = 18;

		public ICommand StartAdvertisingButton { get; }
		public ICommand StopAdvertisingButton { get; }
		public ICommand StartIrmaSessionButton { get; }

		private string statusText;
		public string StatusText
		{
			get
			{
				return this.statusText;
			}

			set
			{
				if (value != this.statusText)
				{
					this.statusText = value;
					InvokePropertyChanged("StatusText");
				}
			}
		}

		private string sendMessage;
		public string SendMessage
		{
			get
			{
				return this.sendMessage;
			}

			set
			{
				if (value != this.sendMessage)
				{
					this.sendMessage = value;
					InvokePropertyChanged("SendMessage");
				}
			}
		}

		private string serverURLText;
		public string ServerURLText
		{
			get
			{
				return this.serverURLText;
			}

			set
			{
				if (value != this.serverURLText)
				{
					this.serverURLText = value;
					InvokePropertyChanged("ServerURLText");
				}
			}
		}

		private bool startBtnEnabled;
		public bool StartBtnEnabled
		{
			get
			{
				return this.startBtnEnabled;
			}

			set
			{
				if (value != this.startBtnEnabled)
				{
					this.startBtnEnabled = value;
					InvokePropertyChanged("StartBtnEnabled");
				}
			}
		}

		private bool sendBtnEnabled;
		public bool SendBtnEnabled
		{
			get
			{
				return this.sendBtnEnabled;
			}

			set
			{
				if (value != this.sendBtnEnabled)
				{
					this.sendBtnEnabled = value;
					InvokePropertyChanged("SendBtnEnabled");
				}
			}
		}

		private bool stopBtnEnabled;
		public bool StopBtnEnabled
		{
			get
			{
				return this.stopBtnEnabled;
			}

			set
			{
				if (value != this.stopBtnEnabled)
				{
					this.stopBtnEnabled = value;
					InvokePropertyChanged("StopBtnEnabled");
				}
			}
		}

		private string sessiontoken = "";
		private IRMABroadcaster broadcaster;
		private List<String> partsToSend = new List<string>();
		private int currentCount = 1;

		/// <summary>
		/// Constructor for the main window, sets the click listeners for the buttons
		/// </summary>
		public MainWindowViewModel()
		{
			//Set click listeners for buttons in main window
			StartAdvertisingButton = new RelayCommand(click => ProcessClickEvent(OnStartAdvertisingButton));
			StopAdvertisingButton = new RelayCommand(click => ProcessClickEvent(OnStopAdvertisingButton));
			StartIrmaSessionButton = new RelayCommand(click => ProcessClickEvent(OnStartIRMASession));

			//Set enabled status of buttons to initial state
			StartBtnEnabled = true;
			SendBtnEnabled = false;
			StopBtnEnabled = false;

			StatusText = "Status";

			//Prefill this URL in server URL (mainly for testing), can also serve as placeholder
			ServerURLText = "http://192.168.178.17:8088/irma_api_server/api/v2/";
		}

		/// <summary>
		/// Initializes the IRMA request at the IRMA server and receives the session token as a response
		/// </summary>
		private void OnStartIRMASession()
		{
			//Create a new IRMA client to talk to IRMA server
			IRMAClient client = new IRMAClient(ServerURLText);

			//Start IRMA request on seperate thread so the UI thread is not blocked
			CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
				async () =>
				{
					try
					{
						sessiontoken = await client.StartIRMARequest(360);
						Debug.WriteLine("Session token: " + sessiontoken);
						SendBtnEnabled = true;
						StartBtnEnabled = false;
						StatusText = "Session token created successfully";
					} catch(Exception e)
					{
						Debug.WriteLine("Error in creating session token");
						SendBtnEnabled = false;
						StartBtnEnabled = true;
						StatusText = "Error occured with creating session token, please make sure URL is valid";
					}
				});
			
		}

		/// <summary>
		/// Starts advertising the server URL and session token
		/// </summary>
		private void OnStartAdvertisingButton()
		{
			Debug.WriteLine("Clicked start advertising");

			//Check if a broadcaster is already sending messages
			if (broadcaster != null)
			{
				if (broadcaster.IsSending())
				{
					{
						broadcaster.StopBroadcast();
					}
				}
			}

			//Start a new broadcaster
			broadcaster = new IRMABroadcaster(this);

			//Start advertising if a sessiontoken has been received
			if (!String.IsNullOrEmpty(sessiontoken))
			{
				ServerURLText += sessiontoken;
				PrepareMessageToSend();

				//Start sending the message in a loop
				broadcaster.SendMessageInLoopAsync(partsToSend);
				SendBtnEnabled = false;
				StopBtnEnabled = true;
			}
		}

		/// <summary>
		/// Stops advertising
		/// </summary>
		private void OnStopAdvertisingButton()
		{
			Debug.WriteLine("Clicked stop advertising");
			broadcaster.StopBroadcast();

			//Reset send message text. Use async because UI thread is different thread
			CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
			() => {
				SendMessage = "";
				StopBtnEnabled = false;
				StartBtnEnabled = true;
			});
		}

		/// <summary>
		/// Checks Server URL and splits this into messages which can be advertised
		/// </summary>
		private void PrepareMessageToSend()
		{
			Debug.WriteLine("Sending server URL: " + ServerURLText);
			currentCount = 1;

			//Check if something is entered as Server URL
			if (String.IsNullOrEmpty(ServerURLText))
			{
				CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
				() =>
				{
					StatusText = "Empty URL";
				}
				);
			} else
			{
				//Remove http:// from url to save bytes
				var shortUrl = ServerURLText.Substring(ServerURLText.IndexOf('/') + 2);
				//Divide the url in parts, since Bluetooth advertisement can only handle 20 bytes per advertisement
				partsToSend = Split(shortUrl, MESSAGE_SIZE).ToList();
				//Make sure the last part is the end string, so mobile app will now when all is received
				partsToSend.Add(currentCount + "." + END_URL);
			}
		}

		/// <summary>
		/// Helper function for dividing the URL string into several parts
		/// </summary>
		/// <param name="stringToSplit">The string which is split</param>
		/// <param name="chunkSize">Size of each part of the string</param>
		/// <returns>List of string parts</returns>
		private IEnumerable<string> Split(string stringToSplit, int chunkSize)
		{
			//Divide string and concatenate current count
			return Enumerable.Range(0, (stringToSplit.Length + chunkSize - 1) / chunkSize)
				.Select(i => currentCount++ + "." 
				+ stringToSplit.Substring(i * chunkSize, Math.Min(stringToSplit.Length - i * chunkSize, chunkSize)));
		}
	}
}
