﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;

namespace IRMA_BLE_UWP.ViewModels
{
	public abstract class BaseViewModel : INotifyPropertyChanged, INotifyCollectionChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;
		public event NotifyCollectionChangedEventHandler CollectionChanged;


		protected void Process<T>(Action<object, T> handler, T e)
		{
			try
			{
				handler(this, e);
			}
			catch (Exception ex)
			{
			}
		}

		/// <summary>
		/// Processes the click event.
		/// </summary>
		/// <param name="action">The function to be called</param>
		protected void ProcessClickEvent(Action action)
		{
			try
			{
				action();
			}
			catch (Exception e)
			{
			}

		}

		/// <summary>
		/// Invokes the PropertyChanged event
		/// </summary>
		/// <param name="property">The changed property</param>
		protected void InvokePropertyChanged(string property)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
		}

		/// <summary>
		/// Invokes the CollectionChanged event
		/// </summary>
		/// <param name="collection">The changed collection</param>
		protected void InvokeCollectionChanged(object collection)
		{
			CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, collection));
		}
	}
}
