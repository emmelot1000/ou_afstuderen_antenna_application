﻿using JWT.Algorithms;
using JWT.Builder;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IRMA_BLE_UWP.Models
{
	/// <summary>
	/// Client for communication with IRMA API server
	/// </summary>
	public class IRMAClient
	{
		private HttpClient client;
		private string ServerUrl { get; set; }

		/// <summary>
		/// Constructor for IRMAClient
		/// </summary>
		/// <param name="url">The URL of the IRMA API server</param>
		public IRMAClient(string url)
		{
			ServerUrl = url;
			client = new HttpClient();
		}

		/// <summary>
		/// Initialize the request jwt and send to IRMA API server
		/// <paramref name="timeout"/>How long (in seconds) the session will be available
		/// </summary>
		public async Task<string> StartIRMARequest(int timeout)
		{
			//Create the payload of the content
			//For now: always request age attribute. This can be made configurable in UI at later times
			var contentPayload = new Dictionary<string, object>
			{
				{ "label", "18+" },
				//{ "attributes", new string[] { "irma-demo.MijnOverheid.ageLower.over18" } }
				{"attributes", new string[] { "irma-demo.idin.ageLimits.over18"} }
			};

			//Create the payload of the request
			var requestPayload = new Dictionary<string, object>
			{
				{ "content", new object[] { contentPayload } }
			};

			//Create the request
			var request = new Dictionary<string, object>
			{
				{ "request", requestPayload },
				{ "validity", 600 },
				{ "timeout", timeout },
				{ "data", "irma" }
			};

			var currentTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();

			//Use a randomly created secret for the request
			const string secret = "GQDstcKsx0NHjPOuXOYg5MbeJ1XT0uFiwDVvVBrk";

			//Create the JWT token which will contain the request for the given attributes
			var token = new JwtBuilder()
				.WithAlgorithm(new HMACSHA256Algorithm())
				.WithSecret(secret)
				.AddClaim("sub", "verification_request")
				.AddClaim("iss", "testsp")
				.AddClaim("iat", currentTime)
				.AddClaim("sprequest", request)
				.Build();

			//Create the broadcast URL
			var broadcastURL = ServerUrl + "broadcast";

			//Send the request and wait for a response of the IRMA server
			var data = await SendRequestAsync(broadcastURL, token);

			//Parse the received response and determine the session token
			JToken receivedToken = JObject.Parse(data);
			JToken receivedSessionToken = receivedToken.SelectToken("u");
			return receivedSessionToken.ToString();
		}

		/// <summary>
		/// Sends a request to the IRMA API server
		/// </summary>
		/// <param name="broadcastURL">The URL to which to send the request</param>
		/// <param name="token">The JWT which contains the broadcast data</param>
		/// <returns></returns>
		private async Task<string> SendRequestAsync(string broadcastURL, string token)
		{
			//Send the new IRMA request to the server to create the session
			using (var client = new HttpClient())
			{
				//Post the token to the IRMA api server
				using (HttpResponseMessage response = await client.PostAsync(
					broadcastURL,
					 new StringContent(token, Encoding.UTF8, "application/json")))
					{
						if (!response.IsSuccessStatusCode)
						{
							Debug.WriteLine("Error in response");
						}
						//Wait for the response of the server
						using (HttpContent content = response.Content)
						{
							var data = await content.ReadAsStringAsync().ConfigureAwait(false);
							return data;
						}
				}
			}
		}
	}


}
