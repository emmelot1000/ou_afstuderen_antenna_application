﻿using IRMA_BLE_UWP.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Devices.Bluetooth.Advertisement;
using Windows.Storage.Streams;
using Windows.UI.Core;

namespace IRMA_BLE_UWP.Models
{
	/// <summary>
	/// Class which handles the BLE advertising
	/// </summary>
	public class IRMABroadcaster
	{
		private BluetoothLEAdvertisementPublisher publisher;
		private MainWindowViewModel viewModel;
		private List<String> partsToSend = new List<string>();
		private bool sending = false;
		private int messageCount = 0;
		private const int TIMER = 7;	//number of seconds between each broadcast

		/// <summary>
		/// Initialize with MainWindowViewModel, so status can be shown and updated in main window
		/// </summary>
		/// <param name="viewModel">The view model which is used for updating status</param>
		public IRMABroadcaster(MainWindowViewModel viewModel) 
		{
			this.viewModel = viewModel;
		}
		
		/// <summary>
		/// Broadcasts the message which is divided in parts in a loop
		/// </summary>
		/// <param name="parts">The parts which are advertised</param>
		/// <returns></returns>
		public async Task SendMessageInLoopAsync(List<String> parts)
		{
			//Remove previous elements from list and resets values
			partsToSend.Clear();
			messageCount = 0;
			partsToSend = parts;
			sending = true;

			while (sending)
			{
				Debug.WriteLine("In loop: " + messageCount);
				try
				{
					//Create a new publisher for each message
					publisher = new BluetoothLEAdvertisementPublisher();
					//Update status when changed
					publisher.StatusChanged += StatusEventHandler;

					// Add custom data to the advertisement
					var manufacturerData = new BluetoothLEManufacturerData
					{
						CompanyId = 0xFFFE
					};

					var writer = new DataWriter();

					//If all messages are already sent once, restart the loop
					if (messageCount == partsToSend.Count)
					{
						//Reset message cound so messages are sent in loop
						messageCount = 0;
					}
					var message = partsToSend.ElementAt(messageCount);
					writer.WriteString(message);

					Debug.WriteLine("Sending message " + messageCount + ": " + message);

					//Update main window to show message which is currently sent. Use async because UI thread is different thread
					CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
					() =>
					{
						viewModel.SendMessage = message;
					});

					// Make sure that the buffer length can fit within an advertisement payload (~20 bytes). 
					manufacturerData.Data = writer.DetachBuffer();
					Debug.WriteLine("length: " + manufacturerData.Data.Length);

					messageCount++;

					// Add the manufacturer data to the advertisement publisher:
					publisher.Advertisement.ManufacturerData.Add(manufacturerData);

					//Start publishing the advertisement
					publisher.Start();

					//Wait for time before sending next message
					await Task.Delay(TimeSpan.FromSeconds(TIMER));
					publisher.Stop();
				}
				catch (Exception e)
				{
					Debug.WriteLine(e);
					publisher.Stop();
					sending = false;
				}
			}
		}

		/// <summary>
		/// Stops the broadcast of the message
		/// </summary>
		public void StopBroadcast()
		{
			sending = false;
			publisher.Stop();
		}

		/// <summary>
		/// Returns the sending state of the current broadcaster
		/// </summary>
		/// <returns></returns>
		public bool IsSending()
		{
			return sending;
		}

		/// <summary>
		/// Listens for status updates of publisher
		/// </summary>
		/// <param name="sender">Initiator of event</param>
		/// <param name="args">Event arguments</param>
		private void StatusEventHandler(BluetoothLEAdvertisementPublisher sender, BluetoothLEAdvertisementPublisherStatusChangedEventArgs args)
		{
			Debug.WriteLine(args.Status.ToString());
			//Update status of the broadcaster. Use async because UI thread is different thread
			CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
			() =>
			{
				viewModel.StatusText = args.Status.ToString();
			});
		}
	}
}
